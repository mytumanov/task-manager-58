package ru.mtumanov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.mtumanov.tm.api.endpoint.IAuthEndpoint;
import ru.mtumanov.tm.api.endpoint.IUserEndpoint;
import ru.mtumanov.tm.dto.model.UserDTO;
import ru.mtumanov.tm.dto.request.user.*;
import ru.mtumanov.tm.dto.response.user.*;

import static org.junit.Assert.*;

public class UserEndpointTest {

    @NotNull
    private final static String host = "localhost";

    @NotNull
    private final static String port = "6060";

    @NotNull
    private static final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(host, port);

    @NotNull
    private static final IUserEndpoint userEndpoint = IUserEndpoint.newInstance(host, port);

    @NotNull
    private static final String admLogin = "COOL_USER";

    @NotNull
    private static final String admPassword = "cool";

    @NotNull
    private static final String userLogin = "USER_FOR_TEST";

    @NotNull
    private static final String userPassword = "test";

    @NotNull
    private static String token;

    @NotNull
    private static String adminToken;

    @NotNull
    private static UserDTO user;

    @BeforeClass
    public static void login() {
        @NotNull final UserLoginRs adminResponse = authEndpoint.login(new UserLoginRq(admLogin, admPassword));
        adminToken = adminResponse.getToken();
    }

    @Before
    public void createUser() {
        @NotNull final UserRegistryRs response = userEndpoint
                .userRegistry(new UserRegistryRq("", userLogin, userPassword, "test@email.ru"));
        user = response.getUser();
        @NotNull final UserLoginRq request = new UserLoginRq(userLogin, userPassword);
        @NotNull final UserLoginRs loginResponse = authEndpoint.login(request);
        token = loginResponse.getToken();
    }

    @After
    public void removeUser() {
        userEndpoint.userRemove(new UserRemoveRq(adminToken, userLogin));
    }

    @Test
    public void testUserChangePassword() {
        @NotNull final String newPassword = "new password";
        @NotNull final UserChangePasswordRs response = userEndpoint.userChangePassword(new UserChangePasswordRq(token, newPassword));
        assertNotEquals(user.getPasswordHash(), response.getUser().getPasswordHash());
    }

    @Test
    public void testErrUserChangePassword() {
        @NotNull UserChangePasswordRs response;
        response = userEndpoint.userChangePassword(new UserChangePasswordRq(token, ""));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());

        response = userEndpoint.userChangePassword(new UserChangePasswordRq("", "newPassword"));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());
    }

    @Test
    public void testUserLock() {
        @NotNull UserProfileRs response;
        response = userEndpoint.userProfile(new UserProfileRq(token));
        assertFalse(response.getUser().isLocked());

        userEndpoint.userLock(new UserLockRq(adminToken, userLogin));
        response = userEndpoint.userProfile(new UserProfileRq(token));
        assertTrue(response.getUser().isLocked());
    }

    @Test
    public void testErrUserLock() {
        @NotNull final UserLockRs response = userEndpoint.userLock(new UserLockRq(adminToken, ""));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());
    }

    @Test
    public void testUserProfile() {
        @NotNull UserProfileRs response = userEndpoint.userProfile(new UserProfileRq(token));
        assertEquals(user, response.getUser());
    }

    @Test
    public void testErrUserProfile() {
        @NotNull UserProfileRs response = userEndpoint.userProfile(new UserProfileRq(""));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());
    }

    @Test
    public void testUserRegistry() {
        @NotNull final String newUserLogin = "very login";
        @NotNull final String newUserPassword = "very password";
        @NotNull final UserRegistryRs response = userEndpoint
                .userRegistry(new UserRegistryRq("", newUserLogin, newUserPassword, "test@email.ru"));
        assertEquals(newUserLogin, response.getUser().getLogin());
        userEndpoint.userRemove(new UserRemoveRq(adminToken, newUserLogin));
    }

    @Test
    public void testErrUserRegistry() {
        @NotNull final String newUserLogin = "very login";
        @NotNull final String newUserPassword = "very password";
        @NotNull UserRegistryRs response;
        response = userEndpoint.userRegistry(new UserRegistryRq("", "", newUserPassword, "test@email.ru"));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());

        response = userEndpoint.userRegistry(new UserRegistryRq("", newUserLogin, "", "test@email.ru"));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());

        response = userEndpoint.userRegistry(new UserRegistryRq("", newUserLogin, newUserPassword, "test@email.ru"));
        response = userEndpoint.userRegistry(new UserRegistryRq("", newUserLogin, newUserPassword, "test@email.ru"));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());

        userEndpoint.userRemove(new UserRemoveRq(adminToken, newUserLogin));
    }

    @Test
    public void testUserRemove() {
        userEndpoint.userRemove(new UserRemoveRq(adminToken, userLogin));
        @NotNull final UserLoginRq request = new UserLoginRq(userLogin, userPassword);
        @NotNull final UserLoginRs loginResponse = authEndpoint.login(request);
        assertFalse(loginResponse.getSuccess());
        assertNull(loginResponse.getToken());
    }

    @Test
    public void testErrUserRemove() {
        @NotNull UserRemoveRs response;
        response = userEndpoint.userRemove(new UserRemoveRq(token, userLogin));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());

        response = userEndpoint.userRemove(new UserRemoveRq(token, ""));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());
    }

    @Test
    public void testUserUnlock() {
        @NotNull UserProfileRs response;
        response = userEndpoint.userProfile(new UserProfileRq(token));
        assertFalse(response.getUser().isLocked());

        userEndpoint.userLock(new UserLockRq(adminToken, userLogin));
        response = userEndpoint.userProfile(new UserProfileRq(token));
        assertTrue(response.getUser().isLocked());

        userEndpoint.userUnlock(new UserUnlockRq(adminToken, userLogin));
        response = userEndpoint.userProfile(new UserProfileRq(token));
        assertFalse(response.getUser().isLocked());
    }

    @Test
    public void testErrUserUnlock() {
        @NotNull UserUnlockRs response;
        response = userEndpoint.userUnlock(new UserUnlockRq("", userLogin));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());

        response = userEndpoint.userUnlock(new UserUnlockRq(adminToken, ""));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());
    }

    @Test
    public void testUserUpdate() {
        @NotNull final String firstName = "new firstName";
        @NotNull final String lastName = "new lastName";
        @NotNull final String middleName = "new middleName";
        userEndpoint.userUpdate(new UserUpdateRq(adminToken, firstName, lastName, middleName));
        @NotNull UserProfileRs response = userEndpoint.userProfile(new UserProfileRq(token));
        assertEquals(user, response.getUser());
    }
}
