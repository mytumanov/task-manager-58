package ru.mtumanov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.mtumanov.tm.api.endpoint.IProjectEndpoint;
import ru.mtumanov.tm.dto.model.ProjectDTO;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.listener.AbstractListener;

@Component
public abstract class AbstractProjectListener extends AbstractListener {

    @NotNull
    protected IProjectEndpoint getProjectEndpoint() {
        return serviceLocator.getProjectEndpoint();
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    protected void showProject(@Nullable final ProjectDTO project) {
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + project.getStatus());
    }

    @Override
    @Nullable
    public Role[] getRoles() {
        return Role.values();
    }

}
