package ru.mtumanov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.listener.AbstractListener;

import java.util.Collection;

public interface ICommandService {

    @NotNull
    Collection<AbstractListener> getTerminalCommands();

    void add(@Nullable AbstractListener abstractCommand);

    @Nullable
    AbstractListener getCommandByName(@NotNull String name);

    @Nullable
    AbstractListener getCommandByArgument(@NotNull String arg);

    @NotNull
    Collection<AbstractListener> getCommandsWithArgument();

}
