package ru.mtumanov.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.request.AbstractUserRq;

@Getter
@Setter
@NoArgsConstructor
public class TaskShowByProjectIdRq extends AbstractUserRq {

    @Nullable
    private String projectId;

    public TaskShowByProjectIdRq(@Nullable final String token, @Nullable final String projectId) {
        super(token);
        this.projectId = projectId;
    }

}
