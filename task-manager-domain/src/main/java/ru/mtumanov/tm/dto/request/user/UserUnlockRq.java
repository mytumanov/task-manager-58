package ru.mtumanov.tm.dto.request.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.request.AbstractUserRq;

@Getter
@Setter
@NoArgsConstructor
public final class UserUnlockRq extends AbstractUserRq {

    @Nullable
    private String login;

    public UserUnlockRq(@Nullable final String token, @Nullable final String login) {
        super(token);
        this.login = login;
    }

}