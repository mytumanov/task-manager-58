package ru.mtumanov.tm.exception.field;

public final class IdEmptyException extends AbstractFieldException {

    public IdEmptyException() {
        super("ERROR! Id is empty!");
    }

}
