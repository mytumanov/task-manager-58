package ru.mtumanov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import ru.mtumanov.tm.api.repository.model.ITaskRepository;
import ru.mtumanov.tm.api.service.model.ITaskService;
import ru.mtumanov.tm.enumerated.Status;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.field.IdEmptyException;
import ru.mtumanov.tm.exception.field.NameEmptyException;
import ru.mtumanov.tm.exception.user.UserIdEmptyException;
import ru.mtumanov.tm.model.Task;
import ru.mtumanov.tm.model.User;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

public class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    @NotNull
    @Autowired
    private ApplicationContext context;

    @Override
    @NotNull
    protected ITaskRepository getRepository() {
        return context.getBean(ITaskRepository.class);
    }

    @Override
    @NotNull
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) throws AbstractException {
        if (projectId.isEmpty())
            return Collections.emptyList();
        if (userId.isEmpty())
            throw new UserIdEmptyException();

        return getRepository().findAllByProjectId(userId, projectId);
    }

    @Override
    @NotNull
    public Task create(@NotNull String userId, @NotNull final String name, @NotNull final String description) throws AbstractException {
        if (userId.isEmpty())
            throw new UserIdEmptyException();
        if (name.isEmpty())
            throw new NameEmptyException();

        @NotNull final User user = getUserRepository().findOneById(userId);
        @NotNull final Task task = new Task();
        @NotNull final ITaskRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        task.setName(name);
        task.setDescription(description);
        task.setUser(user);
        try {
            entityManager.getTransaction().begin();
            repository.add(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

    @Override
    @NotNull
    public Task updateById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final String name,
            @NotNull final String description
    ) throws AbstractException {
        if (id.isEmpty())
            throw new IdEmptyException();
        if (userId.isEmpty())
            throw new UserIdEmptyException();
        if (name.isEmpty())
            throw new NameEmptyException();

        @NotNull final Task task = getRepository().findOneById(userId, id);
        @NotNull final ITaskRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        task.setName(name);
        task.setDescription(description);
        try {
            entityManager.getTransaction().begin();
            repository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

    @Override
    @NotNull
    public Task changeTaskStatusById(@NotNull final String userId, @NotNull final String id, @NotNull final Status status) throws AbstractException {
        if (id.isEmpty())
            throw new IdEmptyException();
        if (userId.isEmpty())
            throw new UserIdEmptyException();

        @NotNull final Task task = getRepository().findOneById(userId, id);
        @NotNull final ITaskRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        task.setStatus(status);
        try {
            entityManager.getTransaction().begin();
            repository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

}
