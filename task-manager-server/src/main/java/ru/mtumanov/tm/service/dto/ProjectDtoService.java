package ru.mtumanov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.mtumanov.tm.api.repository.dto.IDtoProjectRepository;
import ru.mtumanov.tm.api.service.dto.IDtoProjectService;
import ru.mtumanov.tm.dto.model.ProjectDTO;
import ru.mtumanov.tm.enumerated.Status;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.field.IdEmptyException;
import ru.mtumanov.tm.exception.field.NameEmptyException;
import ru.mtumanov.tm.exception.field.StatusNotSupportedException;
import ru.mtumanov.tm.exception.user.UserIdEmptyException;

import javax.persistence.EntityManager;

@Service
public class ProjectDtoService extends AbstractDtoUserOwnedService<ProjectDTO, IDtoProjectRepository>
        implements IDtoProjectService {

    @NotNull
    @Autowired
    private ApplicationContext context;

    @Override
    @NotNull
    protected IDtoProjectRepository getRepository() {
        return context.getBean(IDtoProjectRepository.class);
    }

    @Override
    @NotNull
    public ProjectDTO create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) throws AbstractException {
        if (userId == null || userId.isEmpty())
            throw new UserIdEmptyException();
        if (name == null || name.isEmpty())
            throw new NameEmptyException();

        @NotNull final IDtoProjectRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);

        try {
            entityManager.getTransaction().begin();
            repository.add(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

    @Override
    @NotNull
    public ProjectDTO updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (id == null || id.isEmpty())
            throw new IdEmptyException();
        if (userId == null || userId.isEmpty())
            throw new UserIdEmptyException();
        if (name == null || name.isEmpty())
            throw new NameEmptyException();

        @NotNull final IDtoProjectRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @NotNull final ProjectDTO project = getRepository().findOneById(userId, id);
        project.setName(name);
        project.setDescription(description);

        try {
            entityManager.getTransaction().begin();
            repository.update(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

    @Override
    @NotNull
    public ProjectDTO changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws AbstractException {
        if (id == null || id.isEmpty())
            throw new IdEmptyException();
        if (userId == null || userId.isEmpty())
            throw new UserIdEmptyException();
        if (status == null)
            throw new StatusNotSupportedException();

        @NotNull final IDtoProjectRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @NotNull final ProjectDTO project = repository.findOneById(userId, id);
        project.setStatus(status);
        try {
            entityManager.getTransaction().begin();
            repository.update(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

}
