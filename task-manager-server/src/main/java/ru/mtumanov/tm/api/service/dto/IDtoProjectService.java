package ru.mtumanov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.model.ProjectDTO;
import ru.mtumanov.tm.enumerated.Status;
import ru.mtumanov.tm.exception.AbstractException;

public interface IDtoProjectService extends IDtoUserOwnedService<ProjectDTO> {

    @NotNull
    ProjectDTO create(@Nullable String userId, @Nullable String name, @Nullable String description) throws AbstractException;

    @NotNull
    ProjectDTO updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description) throws AbstractException;

    @NotNull
    ProjectDTO changeProjectStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status) throws AbstractException;

}
